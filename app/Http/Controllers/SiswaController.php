<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
	public function index()
	{
		$data['result'] = \App\Siswa::all();
  	 	return view('siswa/index')->with($data);

	}

	public function create()
	{
		return view('siswa/form');
	}

	public function store(Request $re)
	{
		$rules = [
			'nis'			=> 'required|unique:t_siswa',
			'nama_lengkap'	=> 'required|max:100',
			'jenis_kelamin'	=> 'required',
			'alamat'		=> 'required',
			'no_hp'			=> 'required',
			'id_kelas'		=> 'required|exists:t_kelas',
			'foto'			=> 'required|mimes:jpeg,png|max:512'	
		];

		$this->validate($re,$rules);

		$input = $re->all();
		
		if($re->hasFile('foto') && $re->file('foto')->isValid()){
			$filename = $input['nis']. "." . $re->file('foto')->getClientOriginalExtension();
			$re->file('foto')->storeAs('',$filename);
			$input['foto'] = $filename;
		}

		$status = \App\Siswa::create($input);

		if($status) 
			return redirect('siswa/')->with('success','Data Berhasil Ditambahkan');
		else
			return redirect('siswa/')->with('error','Data Gagal Ditambahkan');
	}

	public function edit($id)
	{
		$data['result'] = \App\Siswa::where('nis',$id)->first();
		return view('siswa/form')->with($data);
	}

	public function update(Request $re,$id)
	{
		$rules = [
			'nama_lengkap'	=> 'required|max:100',
			'jenis_kelamin'	=> 'required',
			'alamat'		=> 'required',
			'no_hp'			=> 'required',
			'id_kelas'		=> 'required|exists:t_kelas',
			'foto'			=> 'mimes:jpeg,png|max:512'
		];

		$this->validate($re,$rules);
		$input = $re->all();

		if($re->hasFile('foto') && $re->file('foto')->isValid()){
			$filename = $input['nis']. "." . $re->file('foto')->getClientOriginalExtension();
			$re->file('foto')->storeAs('',$filename);
			$input['foto'] = $filename;
		}
		
		$result = \App\Siswa::where('nis',$id)->first();
		$status = $result->update($input);

		if($status) 
			return redirect('siswa/')->with('success','Data Berhasil Diubah');
		else
			return redirect('siswa/')->with('error','Data Gagal Diubah');

	}

	public function destroy(Request $re,$id)
	{
		$result = \App\Siswa::where('nis',$id)->first();
		$status = $result->delete();

		if($status) 
			return redirect('siswa/')->with('success','Data Berhasil Dihapus');
		else
			return redirect('siswa/')->with('error','Data Gagal Dihapus');
	}
}


