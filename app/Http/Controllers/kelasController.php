<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class kelasController extends Controller
{
	public function index()
	{
		$data['result'] = \App\Kelas::all();
		return view('kelas/index')->with($data);
	}

	public function create(){
		return view('kelas/form');
	}

	public function store(Request $r)
	{
		$rules = [
			'nama_kelas'	=> 'required|max:100',
			'jurusan'		=> 'required|max:100'
		];

		$this->validate($r,$rules);

		$input = $r->all();
		// $status = DB::table('t_kelas')->insertGetId([]);
		$status = \App\Kelas::create($input);
		if($status)
			return redirect('/')->with('success','Data Berhasil ditambahkan');
		else
			return redirect('/')->with('error','Data Gagal ditambahkan');
		
	}

	//edit

	public function edit($id){
		$data['result'] = \App\Kelas::where('id_kelas',$id)->first();
		return view('kelas/form')->with($data);
	}

	public function update(Request $re, $id)
	{
		$rules = [
			'nama_kelas'	=> 'required|max:100',
			'jurusan'		=> 'required|max:100'
		];

		$this->validate($re,$rules);

		$input = $re->all();
		$result = \App\Kelas::where('id_kelas',$id)->first();
		$status = $result->update($input);

		if($status)
			return redirect('/')->with('success','Data Berhasil diupdate');
		else
			return redirect('/')->with('error','Data Gagal diupdate');
	}

	public function destroy(Request $re,$id)
	{
		$result = \App\Kelas::where('id_kelas',$id)->first();
		$status = $result->delete($result);

		if($status)
			return redirect('/')->with('success','Data Berhasil dihapus');
		else
			return redirect('/')->with('error','Data Gagal dihapus');
	}
}
