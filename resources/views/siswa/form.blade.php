@extends('templates/header')

@section('content')
    <section class="content-header">
      <h1>
       {{ empty($result) ? 'Tambah' : 'Edit'}} Data Siswa
        <small>SMKN 4 BANDUNG</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Data Siswa</li>
        <li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Siswa</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
@include('templates/feedback')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a class="btn btn-purple" href="{{ url('siswa/') }}"><i class="fa fa-arrow-left"></i> Kembali</a>
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form action="{{ empty($result) ? url('siswa/add') : url("siswa/$result->nis/edit") }}"  method="POST" role="form" enctype="multipart/form-data">
            {{ csrf_field() }}

            @if (!empty($result))
              {{method_field('patch')}}
            @endif

            <div class="form-group">
              <label for="" class="control-label">Nis</label>
              <input type="number" class="form-control" id="" placeholder="NIS" name="nis"  value="{{@$result->nis}}">
            </div>

            <div class="form-group">
              <label for="" class="control-label">Nama lengkap</label>
              <input type="text" class="form-control" id="" placeholder="Nama lengkap" value="{{@$result->nama_lengkap}}" name="nama_lengkap">
            </div>     

             <div class="form-group">
              <label for="" class="control-label">Jenis Kelamin</label>
              <div class="checkbox">
                <label><input type="radio" name="jenis_kelamin" value="L" {{(@$result->jenis_kelamin == 'L' ? 'checked' : '')}}>  Laki - Laki </label>
                <label><input type="radio" name="jenis_kelamin" value="P" {{(@$result->jenis_kelamin == 'P' ? 'checked' : '')}} > Perempuan </label>
              </div>

              <div class="form-group">
                <label for="" class="control-label">Alamat</label>
                <textarea type="text" class="form-control" id="" placeholder="Alamat" name="alamat">{!! @$result->alamat !!}</textarea>
              </div>   

               <div class="form-group">
                <label for="" class="control-label">No Telp</label>
                <input type="number" class="form-control" id="" placeholder="No Telp" value="{{@$result->no_hp}}" name="no_hp" maxlength="13" minlength="13">
              </div> 


               <div class="form-group">
                <label for="" class="control-label">Kelas</label>
                <select name="id_kelas" class="form-control">
                  @foreach (\App\Kelas::all() as $kelas)
                    <option value="{{$kelas->id_kelas}}" {{@$result->id_kelas == $kelas->id_kelas ? 'selected' : ''}}> {{@$kelas->nama_kelas}} </option>
                  @endforeach
                </select>
              </div>          
              <div class="form-group">
                <label for="" class="control-label">Foto</label>
                <input type="file" name="foto">
              </div>
            </div>       
            
          
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        <!-- /.box-body -->
       <!--  <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection