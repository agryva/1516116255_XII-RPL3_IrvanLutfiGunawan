@extends('templates/header')

@section('content')
    <section class="content-header">
      <h1>
        Data Siswa
        <small>SMKN 4 BANDUNG</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Smkn 4 Bandung</a></li>
        <li class="active">Data Siswa</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @include('templates/feedback')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a class="btn btn-purple" href="{{ url('siswa/add') }}">Tambah</a>
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Foto</th>
                <th>Nama Lengkap</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>No Telp</th>
                <th>Kelas</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($result as $row)
              <tr>
                <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                <td>
                  <img src="{{asset('uploads/'.@$row->foto)}}" class="img img-circle img-index" alt="">
                </td>
                <td>{{ @$row->nama_lengkap }}</td>
                <td>{{ @$row->jenis_kelamin_display }}</td>
                <td>{{ @$row->alamat }}</td>
                <td>{{ @$row->no_hp }}</td>
                <td>{{ @$row->kelas->nama_kelas }}</td>
                <td>
                  
                  <a href="{{ url("siswa/$row->nis/edit") }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil">  </i></a>
<!-- 
                  <a href="{{ url("kelas/$row->id_kelas/delete") }}" class="btn btn-sm btn-danger"><i class="fa fa-trash">  </i></a> -->

                  <form action="{{ url("siswa/$row->nis/delete") }}" method="POST" style="display:inline">
                    
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}

                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>

                  </form>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
       <!--  <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection