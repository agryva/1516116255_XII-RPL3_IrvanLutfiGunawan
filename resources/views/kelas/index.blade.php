
@extends('templates/header')

@section('content')
    <section class="content-header">
      <h1>
        Data Kelas
        <small>SMKN 4 BANDUNG</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Smkn 4 Bandung</a></li>
        <li class="active">Data Kelas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @include('templates/feedback')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a class="btn btn-purple" href="{{ url('kelas/add') }}"> Tambah</a>
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Jurusan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            @foreach($result as $row)
              <tr>
                <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                <td>{{ $row->nama_kelas }}</td>
                <td>{{ $row->jurusan }}</td>
                <td>
                  
                  <a href="{{ url("kelas/$row->id_kelas/edit") }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil">  </i></a>
<!-- 
                  <a href="{{ url("kelas/$row->id_kelas/delete") }}" class="btn btn-sm btn-danger"><i class="fa fa-trash">  </i></a> -->

                  <form action="{{ url("kelas/$row->id_kelas/delete") }}" method="POST" style="display:inline">
                    
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}

                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>

                  </form>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
       <!--  <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection