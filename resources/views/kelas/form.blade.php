@extends('templates/header')

@section('content')
    <section class="content-header">
      <h1>
       {{ empty($result) ? 'Tambah' : 'Edit'}} Data Kelas
        <small>SMKN 4 BANDUNG</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Data Kelas</li>
        <li class="active">{{ empty($result) ? 'Tambah' : 'Edit' }} Data Kelas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
@include('templates/feedback')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a class="btn btn-purple" href="{{ url('/') }}"><i class="fa fa-arrow-left"></i> Kembali</a>
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form action="{{ empty($result) ? url('kelas/add') : url("kelas/$result->id_kelas/edit") }}"  method="POST" role="form">
            {{ csrf_field() }}

            @if (!empty($result))
              {{method_field('patch')}}
            @endif

            <div class="form-group">
              <label for="" class="control-label">Nama kelas</label>
              <input type="text" class="form-control" id="" placeholder="Nama Kelas" name="nama_kelas" value="{{@$result->nama_kelas}}">
            </div>

            <div class="form-group">
              <label for="" class="control-label">Jurusan</label>
              <input type="text" class="form-control" id="" placeholder="Jurusan" value="{{@$result->jurusan}}" name="jurusan">
            </div>         
            
          
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        <!-- /.box-body -->
       <!--  <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection