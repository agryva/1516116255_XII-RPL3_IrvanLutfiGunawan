@if(session('success'))
	<div class="alert alert-success fade in">
		<button class="close pull-right" type="button" data-dismiss="alert" arial-label="Close"> <span arial-hidden="true"></span> </button>
		{!! session('success') !!}
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger fade in">
		<button class="close pull-right" type="button" data-dismiss="alert" arial-label="Close"> <span arial-hidden="true"></span> </button>
		<p>Perhatian</p>

		{!! session('error') !!}
	</div>
@endif

@if(count($errors) > 0)
	<div class="alert alert-danger fade in">
		<button class="close pull-right" type="button" data-dismiss="alert" arial-label="Close"> <span arial-hidden="true"></span> </button>
		<p>Perhatian</p>
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif