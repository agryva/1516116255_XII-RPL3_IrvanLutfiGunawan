<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_siswa',function(Blueprint $tb)
        {
            $tb->integer('nis')->primary();
            $tb->string('nama_lengkap',100);
            $tb->string('jenis_kelamin',1);
            $tb->text('alamat');
            $tb->string('no_hp',13);
            $tb->integer('id_kelas');
            $tb->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_siswa');
    }
}
